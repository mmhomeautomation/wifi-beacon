# wifi-beacon

### What is does
This modules get's called periodicly and checks if the wifi is connected. If not, it launches the wifi-manager.

This module also saves the connection settings for the mqtt server.