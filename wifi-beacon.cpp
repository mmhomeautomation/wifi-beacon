#ifdef __PLATFORM_ESP8266
#include <ESP8266mDNS.h>
#elif defined(__PLATFORM_ESP32)
#include <ESPmDNS.h>
#endif
#include <modules/debug-utils/debug-utils.hpp>
#include <modules/wifi-manager/WiFiManager.h>
#include <modules/configuration-manager/configuration-manager.hpp>
#include <modules/debug-utils/Logger.hpp>
#include "wifi-beacon.hpp"

Logger *logger = new Logger("wifi:beacon", 1, C_CYAN);

#define MQTT_SERVER_LENGTH 48
#define MQTT_TOPIC_LENGTH  48

void wifi_beacon_execute(bool forceStart = false);
void saveConfigCallback(void);
void readin_config(void);

EEPNodeI magicByte;
EEPNode mqttServer(MQTT_SERVER_LENGTH);
EEPNodeI mqttPort;
EEPNode mqttTopic(MQTT_TOPIC_LENGTH);

WiFiManager wifiManager;

bool shouldSaveConfig = false;
char mqtt_server[MQTT_SERVER_LENGTH];
char mqtt_port[6] = "1883";
char mqtt_topic[MQTT_TOPIC_LENGTH];

WiFiManagerParameter custom_mqtt_server("mqtt_server", "mqtt_server", mqtt_server, MQTT_SERVER_LENGTH);
WiFiManagerParameter custom_mqtt_port("mqtt_port", "mqtt_port", mqtt_port, 6);
WiFiManagerParameter custom_mqtt_topic("mqtt_topic", "mqtt_topic", mqtt_topic, MQTT_TOPIC_LENGTH);

extern enum DEVICE_STATE ESP_State;
uint16_t lastStatus = 0;

void saveConfigCallback() {
	logger->printf("Saving new parameter to EEPROM\n");
	shouldSaveConfig = true;
}

bool wifi_beacon_isConnected() {
	return WiFi.status() == WL_CONNECTED;
}

void wifi_beacon_loop() {
	if(ESP_State == MQTT_FAILED) {
		logger->printf("Failed to connect to mqtt server. Restarting captive portal to setup mqtt server\n");
		wifi_beacon_execute(true);
	}
	if( !wifi_beacon_isConnected() ) {
		wifi_beacon_app_disconnected();
		wifi_beacon_execute();
	}
	if(WiFi.status() != lastStatus) {
		#ifdef __PLATFORM_ESP8266
		MDNS.update();
		#endif
		lastStatus = WiFi.status();
	}
}

void readin_config() {
	logger->println("Valid config found. Reading...");
	CFG_setMqttServer( mqttServer.getValue(), mqttServer.getLength() );
	logger->printf( "MQTT server: %s\n", CFG_getMqttServer() );

	CFG_setMqttPort( mqttPort.get() );
	logger->printf( "MQTT server port: %i\n", CFG_getMqttPort() );

	CFG_setMqttBaseTopic( mqttTopic.getValue(), mqttTopic.getLength() );
	logger->printf( "MQTT base topic: %s\n", CFG_getMqttBaseTopic() );
}

void wifi_beacon_setup() {
	lastStatus = WiFi.status();
	logger->printf("Allocating eeprom space...\n");
	magicByte.allocate();
	mqttServer.allocate();
	mqttPort.allocate();
	mqttTopic.allocate();
	wifiManager.setup();
	EEPU_begin();
	CFG_loadDefaults();

	wifiManager.setSaveConfigCallback(saveConfigCallback);
	wifiManager.setTimeout(120);	// after 2min inactivity, retry with old settings
	wifiManager.addParameter(&custom_mqtt_server);
	wifiManager.addParameter(&custom_mqtt_port);
	wifiManager.addParameter(&custom_mqtt_topic);

	uint16_t magicBytes = magicByte.get();
	logger->printfln("Magic bytes: 0x%04X\n", magicBytes);
	if(magicBytes != 1337) {
		logger->println("Found empty storage.");
		wifiManager.resetSettings();
		EEPU_wipe();
		memcpy( mqtt_server, CFG_getMqttServer(), strlen( CFG_getMqttServer() ) );
		memcpy( mqtt_topic, CFG_getMqttBaseTopic(), strlen( CFG_getMqttBaseTopic() ) );
	} else  {
		readin_config();
	}


	logger->printf("Done.\n");
}

void wifi_beacon_execute(bool forceStart) {
	uint16_t magicBytes = magicByte.get();
	logger->printf("Magic bytes: 0x%04X\n", magicBytes);
	if(magicBytes != 1337) {
		logger->printfln("Found empty storage.");
		wifiManager.resetSettings();
		EEPU_wipe();
	} else  {
		readin_config();
	}

	//statusLedSetWifi(false);
	ESP_State = WIFI_CONNECTING;

	memcpy( mqtt_server, CFG_getMqttServer(), strlen( CFG_getMqttServer() ) );
	memcpy( mqtt_topic, CFG_getMqttBaseTopic(), strlen( CFG_getMqttBaseTopic() ) );

	if(forceStart) {
		wifiManager.startConfigPortal();
	} else  {
		wifiManager.autoConnect();
	}

	if (shouldSaveConfig) {
		mqttServer.setValue( custom_mqtt_server.getValue(), strlen( custom_mqtt_server.getValue() ) );
		mqttPort.set( String( custom_mqtt_port.getValue() ).toInt() );
		mqttTopic.setValue( custom_mqtt_topic.getValue(), strlen( custom_mqtt_topic.getValue() ) );

		CFG_setMqttServer( mqttServer.getValue(), mqttServer.getLength() );
		logger->printf( "MQTT server: %s\n", CFG_getMqttServer() );

		CFG_setMqttPort( mqttPort.get() );
		logger->printf( "MQTT server püort: %i\n", CFG_getMqttPort() );

		CFG_setMqttBaseTopic( mqttTopic.getValue(), mqttTopic.getLength() );
		logger->printf( "MQTT base topic: %s\n", CFG_getMqttBaseTopic() );

		magicByte.set(1337);

		logger->printf( "Saved Magic bytes: %i\n", magicByte.get() );

		EEPU_flush();
		if(!forceStart) {
			logger->printfln( "%sRestarting ESP to apply changes...", T_FG_COLOR(C_RED) );
			ESP.restart();
		}
	}

	//statusLedSetWifi(true);
	ESP_State = WIFI_CONNECTED;
}
