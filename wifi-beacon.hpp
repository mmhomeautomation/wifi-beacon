#ifndef __WIFI_BEACON_HPP__
#define __WIFI_BEACON_HPP__

#include <modules/app-base/application.hpp>

#define wifi_beacon_app_connected application_connected
#define wifi_beacon_app_disconnected application_disconnected

void wifi_beacon_loop(void);
bool wifi_beacon_isConnected(void);
void wifi_beacon_setup(void);

#endif
